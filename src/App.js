import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { notesSelector } from 'store/selectors';

import actions from 'store/actions';

import Add from 'components/Add';
import Note from 'components/Note';

const enhance = connect(
  createStructuredSelector({
    notes: notesSelector,
  }),
)

function App({ dispatch, notes }) {

  useEffect(() => {
    dispatch({
      type: actions.INIT_STATE,
    })
  }, [dispatch])

  return (
    <div className="notes-container">
      {Object.keys(notes).map(id => (
        <Fragment key={id}>
          <Note {...notes[id]} />
        </Fragment>
      ))}
      <Add />
    </div>
  );
}



export default enhance(App);
