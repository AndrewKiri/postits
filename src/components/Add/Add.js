import React, { useCallback } from 'react';
import { connect } from 'react-redux';

import actions from 'store/actions';

const Add = ({
  dispatch
}) => {
  const onClick = useCallback(
    () => {
      dispatch({
        type: actions.CREATE_NEW_NOTE,
      });
    },
    [dispatch],
  );

  return (
    <div className="new-post-it" onClick={onClick}>
      +
    </div>
  );
};

export default connect()(Add);
