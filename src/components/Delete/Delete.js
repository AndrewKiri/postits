import React, { useCallback } from 'react';
import { connect } from 'react-redux';

import actions from 'store/actions';

const Delete = ({ id, dispatch }) => {
  const onClick = useCallback(
    () => {
      dispatch({
        type: actions.DELETE_NOTE,
        payload: { id },
      })
    },
    [dispatch, id]
  )

  return (
    <div className="delete" onClick={onClick}>
      x
    </div>
  );
};

export default connect()(Delete);
