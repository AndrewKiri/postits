import React, { Component } from 'react'
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';

import actions from 'store/actions'

import Delete from '../Delete';

class Note extends Component {
  constructor(props) {
    super(props);
    const { author, text, date } = props;

    this.state = {
      author: author || '',
      text: text || '',
      date: date || '',
    }
  }

  saveChanges = debounce((payload) => {
    const { dispatch } = this.props;

    dispatch({
      type: actions.SET_VALUE,
      payload,
    })
  }, 300)

  handleChange = e => {
    const { name, value } = e.target;
    const { id } = this.props;

    this.setState({ [name]: value }, () => {
      this.saveChanges({
        id,
        key: name,
        value,
      });
    });
  };

  render() {
    const {
      id,
    } = this.props;
    const {
      author,
      date,
      text,
    } = this.state;

    return (
      <div className="post-it">
        <Delete id={id} />
        <div className="sticky taped">
          <input
            className="author"
            type="text"
            value={author}
            name="author"
            onChange={this.handleChange}
            placeholder={"Author's name"}
          />
          <textarea
            onChange={this.handleChange}
            className="text"
            name="text"
            defaultValue={text}
            placeholder={"Text"}
          />
          <div className="date">
            {date}
          </div>
        </div>
      </div>
    )
  }
}

export default connect()(Note);
