import nanoid from 'nanoid';

const ELDAR = {
  id: nanoid(),
  author: 'Eldar Lazhuey',
  date: new Date('December 17, 1995 03:24:00').toDateString(),
  text: 'Sirena is my love',
};

const ANDREW = {
  id: nanoid(),
  author: 'Andrew',
  date: new Date('June 26, 2012 03:24:00').toDateString(),
  text: 'I love Vika, the massage girl'
};

export default {
  [ELDAR.id]: ELDAR,
  [ANDREW.id]: ANDREW
}

export const treeData = [
  ELDAR,
  ANDREW
]
