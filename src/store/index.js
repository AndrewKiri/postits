import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = composeEnhancers(applyMiddleware());

const store = createStore(reducers, middleware);

export default store;
