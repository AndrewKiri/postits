import produce from 'immer';
import nanoid from 'nanoid';

import { get, set } from 'utils/localStorage';

import actions from '../actions';

const initialState = {
  notes: {},
};

const syncLocalStorage = notes => set({
  keyValuePairs: {
    notes: JSON.stringify(notes),
  }
})

export default (state = initialState, action) => produce(state, (draft) => {
  switch (action.type) {
    case actions.INIT_STATE: {
      draft.notes = JSON.parse(get({ keys: 'notes' })) || {};
      break;
    }
    
    case actions.SET_VALUE: {
      const { id, key, value } = action.payload;

      draft.notes[id][key] = value;
      syncLocalStorage(draft.notes);
      break;
    }

    case actions.CREATE_NEW_NOTE: {
      const newNote = {
        id: nanoid(),
        author: '',
        date: new Date().toDateString(),
        text: '',
      };

      draft.notes[newNote.id] = newNote;
      syncLocalStorage(draft.notes);
      break;
    }

    case actions.DELETE_NOTE: {
      const { id } = action.payload;

      delete draft.notes[id];
      syncLocalStorage(draft.notes);
      break;
    }

    default:
      break;
  }
});
