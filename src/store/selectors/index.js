// import { createSelector } from 'reselect';

export const notesSelector = (state) => state.notes || [];
export const treeDataSelector = (state) => state.treeData || [];
