const { isArray } = Array;

const get = ({ prefix, keys }) => {
  if (isArray(keys)) {
    const keyValuePairs = keys.reduce((acc, item) => {
      const key = prefix != null
        ? `${prefix}.${item}`
        : item;
      acc[item] = localStorage.getItem(String(key));
      return acc;
    }, {});
    return keyValuePairs;
  }
  if (typeof keys === 'string') {
    const key = prefix != null ? `${prefix}.${keys}` : String(keys);
    return localStorage.getItem(key);
  }
};

const set = ({ prefix, keyValuePairs }) => {
  if (keyValuePairs && keyValuePairs.constructor === Object) {
    Object.keys(keyValuePairs).forEach((k) => {
      const key = prefix != null ? `${prefix}.${k}` : k;
      localStorage.setItem(String(key), String(keyValuePairs[k]));
    });
  }
};

const remove = ({ prefix, keys }) => {
  if (isArray(keys)) {
    keys.forEach((k) => {
      const key = prefix != null ? `${prefix}.${k}` : k;
      localStorage.removeItem(String(key));
    });
  }
  if (typeof keys === 'string') {
    const key = prefix != null ? `${prefix}.${keys}` : keys;
    localStorage.removeItem(String(key));
  }
};

const clear = () => localStorage.clear();

export {
  get,
  set,
  remove,
  clear,
};
